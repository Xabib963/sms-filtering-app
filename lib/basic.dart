// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:permission_handler/permission_handler.dart';
// import 'package:telephony/telephony.dart';

// onBackgroundMessage(SmsMessage message) {
//   debugPrint("onBackgroundMessage called");
// }

// void main() {
//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
//         useMaterial3: true,
//       ),
//       home: const MyHomePage(title: 'Flutter Demo Home Page'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   const MyHomePage({super.key, required this.title});

//   final String title;

//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   // static const battaryChanel = MethodChannel('habib/battery');
//   String _message = "";
//   List<SmsMessage>? messages;
//   final telephony = Telephony.instance;

//   Future getMesseges() async {
//     messages = await telephony.getInboxSms(
//         columns: [
//           SmsColumn.ADDRESS,
//           SmsColumn.BODY,
//           SmsColumn.SUBSCRIPTION_ID,
//           SmsColumn.ID,
//           SmsColumn.THREAD_ID,
//           SmsColumn.SERVICE_CENTER_ADDRESS,
//           SmsColumn.DATE,
//         ],
//         filter: SmsFilter.where(SmsColumn.ADDRESS).greaterThanOrEqualTo(
//             "${DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 0).millisecondsSinceEpoch}"),
//         sortOrder: [
//           OrderBy(SmsColumn.DATE, sort: Sort.DESC),
//           // OrderBy(SmsColumn.BODY)
//         ]);
//     setState(() {});
//   }

//   @override
//   void initState() {
//     initPlatformState();
//     getMesseges();

//     Telephony.instance.requestPhoneAndSmsPermissions;
//     super.initState();
//   }

//   onMessage(SmsMessage message) async {
//     // print(DateTime(
//     //         DateTime.now().year, DateTime.now().month, DateTime.now().day, 0)
//     //     .millisecondsSinceEpoch);
//     // telephony.cellularDataState.then((value) => print(value));

//     // telephony.simOperator.then((value) => print(value));
//     // telephony.simOperator.then((value) => print(value));
//     // telephony.networkOperatorName.then((value) => print(value));
//     // telephony.simState.then((value) => print(value.name));
//     // print(message.subscriptionId ?? 'kk');
//     // print(message.body ?? 'kk');

//     setState(() {
//       _message = message.body ?? "Error reading message body.";
//     });
//     await getMesseges();
//   }

//   // onSendStatus(SendStatus status) {
//   //   setState(() {
//   //     _message = status == SendStatus.SENT ? "sent" : "delivered";
//   //   });
//   // }

//   // Platform messages are asynchronous, so we initialize in an async method.
//   Future<void> initPlatformState() async {
//     await Permission.phone.request();
//     await Permission.sms.request();
//     final bool? result = await telephony.requestPhoneAndSmsPermissions;

//     if (result != null && result) {
//       telephony.listenIncomingSms(
//           onNewMessage: onMessage, onBackgroundMessage: onBackgroundMessage);
//     }

//     if (!mounted) return;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Theme.of(context).colorScheme.inversePrimary,
//         title: Text(widget.title),
//       ),
//       body: RefreshIndicator(
//         onRefresh: () async {
//           await getMesseges();

//           setState(() {});
//         },
//         child: ListView.builder(
//           itemCount: messages!.length,
//           itemBuilder: (context, index) {
//             print(
//                 DateTime.now().millisecondsSinceEpoch - messages![index].date!);
//             return ListTile(
//               trailing: (DateTime.now().millisecondsSinceEpoch -
//                           messages![index].date!) <
//                       120000
//                   ? CircleAvatar(
//                       radius: 10,
//                       backgroundColor: Colors.blue,
//                     )
//                   : SizedBox(),
//               leading: Text(messages![index].threadId.toString()),
//               title: Text(messages![index].body.toString()),
//               subtitle: Text(messages![index].subscriptionId.toString() == '0'
//                   ? ' SIM 1'
//                   : 'SIM 2'),
//             );
//           },
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () {},
//         tooltip: 'Increment',
//         child: const Icon(Icons.add),
//       ), // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
// }
