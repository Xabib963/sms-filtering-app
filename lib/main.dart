import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:telephony/telephony.dart';

onBackgroundMessage(SmsMessage message) {
  debugPrint("onBackgroundMessage called");
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const simChanel = MethodChannel('habib/battery');
  String _message = "";
  List<SmsMessage>? messages;
  List<SmsMessage>? messages1;
  final telephony = Telephony.instance;

  Future getPhoneState(String? sim) async {
    messages = await telephony.getInboxSms(
        columns: [
          SmsColumn.ADDRESS,
          SmsColumn.BODY,
          SmsColumn.SUBSCRIPTION_ID,
          SmsColumn.ID,
          SmsColumn.THREAD_ID,
          SmsColumn.SERVICE_CENTER_ADDRESS,
          SmsColumn.DATE,
        ],
        filter: SmsFilter.where(SmsColumn.SUBSCRIPTION_ID).equals(sim ?? '1'),
        sortOrder: [
          OrderBy(SmsColumn.DATE, sort: Sort.DESC),
          OrderBy(SmsColumn.BODY)
        ]);
    setState(() {
      messages1 = messages;
    });
  }

  Future getPhoneStateByMessageBody(String? body) async {}

  @override
  void initState() {
    initPlatformState();
    getPhoneState(null);

    Telephony.instance.requestPhoneAndSmsPermissions;
    super.initState();
  }

  onMessage(SmsMessage message) async {
    // print(DateTime(
    //         DateTime.now().year, DateTime.now().month, DateTime.now().day, 0)
    //     .millisecondsSinceEpoch);
    // telephony.cellularDataState.then((value) => print(value));

    // telephony.simOperator.then((value) => print(value));
    // telephony.simOperator.then((value) => print(value));
    // telephony.networkOperatorName.then((value) => print(value));
    // telephony.simState.then((value) => print(value.name));
    // print(message.subscriptionId ?? 'kk');
    // print(message.body ?? 'kk');

    setState(() {
      _message = message.body ?? "Error reading message body.";
    });
    await getPhoneState(_value);
  }

  onSendStatus(SendStatus status) {
    setState(() {
      _message = status == SendStatus.SENT ? "sent" : "delivered";
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    await Permission.phone.request();
    await Permission.sms.request();
    final bool? result = await telephony.requestPhoneAndSmsPermissions;

    if (result != null && result) {
      telephony.listenIncomingSms(
          onNewMessage: onMessage, onBackgroundMessage: onBackgroundMessage);
    }

    if (!mounted) return;
  }

  String _value = '0';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          DropdownButton(
            items: [
              DropdownMenuItem(
                child: Text('SIM1'),
                value: '0',
              ),
              DropdownMenuItem(
                child: Text('SIM2'),
                value: '1',
              )
            ],
            value: _value,
            onChanged: (value) async {
              _value = value!;
              await getPhoneState(_value);
              setState(() {});
            },
          ),
        ],
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await getPhoneState(_value);

          setState(() {});
        },
        child: ListView.builder(
          itemCount: messages1!.length,
          itemBuilder: (context, index) {
            print(DateTime.now().millisecondsSinceEpoch -
                messages1![index].date!);
            return ListTile(
              trailing: (DateTime.now().millisecondsSinceEpoch -
                          messages1![index].date!) <
                      120000
                  ? CircleAvatar(
                      radius: 10,
                      backgroundColor: Colors.blue,
                    )
                  : SizedBox(),
              leading: Text(messages1![index].threadId.toString()),
              title: Text(messages1![index].body.toString()),
              subtitle: Text(messages1![index].subscriptionId.toString() == '0'
                  ? 'Syriatel SIM 1'
                  : 'MTN SIM 2'),
            );
          },
        ),
      ),
      floatingActionButton: Container(
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: Colors.white, border: Border.all(color: Colors.purple)),
        width: MediaQuery.sizeOf(context).width * 0.8,
        child: TextFormField(
            decoration: const InputDecoration(
                hintText: "Type in your Msgbody...",
                suffixIcon: Icon(Icons.search)),
            onChanged: (value) async {
              messages1 = messages!.where((e) {
                return e.body!.toLowerCase().contains(value.toLowerCase()!);
              }).toList();
              print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
              print(messages1!.first.body!);
              print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
              setState(() {});
            }),
      ),
    );
  }
}
